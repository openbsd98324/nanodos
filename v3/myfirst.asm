	BITS 16
intmain:
    mov ax, 07C0h	; Set up 4k stack
    add ax, 288     ; (4096 + 512) / 16 bytes per paragraph
    mov ss, ax
    mov sp, 4096
    mov ax, 07C0h   ; Set data segment to where we're loaded
    mov ds, ax

    call linefeed
    mov ah,0x0e
    mov al,'-' 
    int 0x10  
    mov al,'M'
    int 0x10  
    mov al,'y'
    int 0x10  
    mov al,'O'
    int 0x10  
    mov al,'S'
    int 0x10 
    mov al,'-'
    int 0x10 
    call linefeed

    ; wait for key
    mov ah,0h               ; read a key once using bios service
    int 0x10

    ; change mode to large fonts 
    mov al, 00h
    mov ah, 0
    int 10h

    ; change cursor to block shape
    mov ch, 0 
    mov cl, 7 
    mov ah, 1 
    int 10h

    ; move cursor to 
    mov dh, 1   ; row 
    mov dl, 1  ; column
    mov bh, 0 
    mov ah, 2 
    int 10h

.myloop:
    call linefeed
    mov si, mypresskey
    call print 

    ; put space
    mov al,' '
    int 0x10 

    mov ah,0h               ; read a key once using bios service
    int 0x10
    int 16h                 ; puts the pressed key into al


    mov si, myhello
    call print 
    mov si, myworld
    call print 
    call linefeed 

    jmp .myloop

    mystring db '---string---', 0
    myhello db 'Hello',0 
    myworld db ' World',0 
    mypresskey db 'Press Key:',0 


linefeed:
    mov ax, 0x0E0D
    int 0x10
    mov ax, 0x0E0A
    int 0x10
    ret

print:
 pusha
 mov  bh, 0       ; BH is DisplayPage (No need for GraphicsColor in BL)
 start:
  mov al, [si]
  cmp al, 0
  je done
  mov ah, 0x0E
  int 0x10
  inc si
  jmp start
 done:
  popa
  ret

.done:
    ret
    times 510-($-$$) db 0; Pad the boot sector with 0s
    dw 0xAA55   ; Standard PC boot signature

